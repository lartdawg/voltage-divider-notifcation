#!/usr/sbin/python

import spidev, string, time, os
from time import gmtime, strftime
import smtplib, sys

# Definitions for getVolts
channel_0 = 0               # ADC Channel 0
channel_1 = 1               # ADC Channel 1
delay = 5               # Delay between readings
measurements = 5               # Number of readings for average value

# Open SPI bus
spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz = 100000

# Function to read SPI data from MCP3002 chip
# Channel must be an integer 0|1

def ReadChannel(channel):
  data = 0
  for i in range(0,measurements):
    adc = spi.xfer2([104,0])
    #adc = spi.xfer2([1,(2+channel)<<6,0])
    data += int(((adc[0]&3) << 8) + adc[1])
    #data += ((adc[1]&31) << 6) + (adc[2] >> 2)
    #time.sleep(0.2)
  data = float(data) / measurements
  return data

def sendMail(x):
  gmail_user = 'xxx@gmail.com'
  gmail_password = 'xxx'
  sent_from = gmail_user
  to = 'xxx@gmail.com'
  subject = 'Solar voltage'

  try:
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    server.sendmail(sent_from, to, x)
    server.close()
    print 'Email sent!'
  except:
    #print email_text
    print 'Something went wrong...'


if __name__ == '__main__':
  #sendMail(str(sys.argv))
  # Read the voltage divider
  level_0 = ReadChannel(channel_0)
  volts_0 = round((level_0 * 3.33) / float(1024) * 4, 2)

  # Print out results from voltage divider
  timenow = str(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
  #print("{} | {} | {}".format(timenow, level_0, volts_0))
  if volts_0 <= 12.10:
    sendMail(str(volts_0))
  else:
    print "nah bru"
